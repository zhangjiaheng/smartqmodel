from flask import Flask, jsonify, request
from sklearn.externals import joblib
import pandas as pd
import numpy as np
import pickle
import json
# from google.appengine.api import app_identity
# import cloudstorage as gcs
# import logging

#new comment

app = Flask(__name__)

with open(f'model/smartqmodel.pkl', 'rb') as f:
    loaded_model = pickle.load(f)
    print("model loaded")

@app.route('/', methods=['POST'])
def main():
    """API Call
    Pandas dataframe (sent as a payload) from API Call
    """
    try:
        receive_json = request.get_json()
        print(type(receive_json))
        test = receive_json[0]
        print(type(test))
    except Exception as e:
        raise e

    if not test:
        return(bad_request())
    else:
        test_array = np.asarray(test).reshape(1,-1)
        prediction = loaded_model.predict(test_array)
        #This is to round the number to the nearest 5 mins
        prediction = 5 * round(prediction.item(0)/5)
        response = jsonify(prediction)
        response.status_code = 200
        return (response)

def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
